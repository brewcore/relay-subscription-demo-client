import React, { Component } from 'react'
import './App.css'
import { graphql } from 'babel-plugin-relay/macro'
import { QueryRenderer, requestSubscription } from 'react-relay'
import environment from './Relay/Environment'
import Message from './Message'
import { ConnectionHandler } from 'relay-runtime'

class App extends Component {
  componentDidMount () {
    const subscriptionConfig = {
      subscription: graphql`
        subscription AppMessagesSubscription {
          messages {
            node {
              ...Message_message
              id
            }
          }
        }
      `,
      // variables: { input: {} },
      // TODO implement reconnect if server closes connection
      onCompleted: data => console.log(data),
      onError: error => console.error(error),
      updater: store => {
        const newRecord = store.getRootField('messages').getLinkedRecord('node')
        const conn = ConnectionHandler.getConnection(
          store.getRoot(),
          'AppQuery_messages'
        )
        const edge = ConnectionHandler.createEdge(store, conn, newRecord, 'newMessageEdge')
        ConnectionHandler.insertEdgeAfter(conn, edge)
      }
    }

    this.subscription = requestSubscription(environment, subscriptionConfig)
  }

  componentWillUnmount () {
    this.subscription.dispose()
  }

  render () {
    return (
      <QueryRenderer
        environment={environment}
        query={graphql`
          query AppQuery{
            messages(last: 10) @connection(key: "AppQuery_messages") {
              edges{
                node{
                  id
                  ...Message_message
                }
              }
            }
          }
        `}
        variables={{}}
        render={({ error, props }) => {
          if (error) {
            console.error(error)
            return <div>Error!</div>
          }
          if (!props) {
            return <div>Loading...</div>
          }
          return <div>
            Messages:
            <ul>
              {props.messages.edges.map(edge => <Message key={edge.node.id} message={edge.node} />)}
            </ul>
          </div>
        }}
      />
    )
  }
}

export default App
