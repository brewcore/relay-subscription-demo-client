import React, { Component } from 'react'
import './App.css'
import { graphql } from 'babel-plugin-relay/macro'
import { createFragmentContainer } from 'react-relay'

class Message extends Component {
  render () {
    const { username, message } = this.props.message
    return (
      <li>
        {username}: {message}
      </li>
    )
  }
}

export default createFragmentContainer(
  Message,
  graphql`
    # As a convention, we name the fragment as '<ComponentFileName>_<propName>'
    fragment Message_message on newMessage {
      username
      message
    }
  `
)
