/* global fetch */
import {
  Environment,
  Network,
  RecordSource,
  Store
} from 'relay-runtime'
import { execute } from 'apollo-link'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import { WebSocketLink } from 'apollo-link-ws'

function fetchQuery (
  operation,
  variables
) {
  const headers = {
    'Content-Type': 'application/json'
  }
  return fetch('http://localhost:4001/graphql', {
    method: 'POST',
    headers,
    body: JSON.stringify({
      query: operation.text,
      variables
    })
  }).then(response => {
    return response.json()
  })
}

const subscriptionClient = new SubscriptionClient('ws://localhost:4001/graphql', {
  reconnect: true
})

const subscriptionLink = new WebSocketLink(subscriptionClient)

// Prepare network layer from apollo-link for graphql subscriptions
const networkSubscriptions = (operation, variables) =>
  execute(subscriptionLink, {
    query: operation.text,
    variables
  })

const environment = new Environment({
  network: Network.create(fetchQuery, networkSubscriptions),
  store: new Store(new RecordSource())
})

export default environment
